# adventofcode-2022

My Advent of Code 2022 attempts.  Languages may vary.

Play along at <https://adventofcode.com/2022>

This repo at <https://gitlab.com/scotek/adventofcode-2022>

# Day 01 `idris/Day01.idr`

## Task 1: 20m

Shaking rust off getting things working again.  As usual the first tasks are straight-forward and only take a moment to think of the solution, then it's random little compiler/syntax/stdlib bumps throwing you off.  Forgot (as with every year) that Idris doesn't have Haskell's `maximum` in the stdlib so it takes a minute to think stop trying to look for something I think I've forgotten about and just do it with a fold!

## Task 2: 9m

Straight-forward.  Main delay for task 2 was `List` vs `List1` from `split`.  I'm not sure why `List1` wouldn't have `sort` defined since if it works for a potentially empty list then it'll definitely work for a list guaranteed to have at least one entry...

### Wrong attempts

6m. Typed wrong variable name without thinking which meant I wasn't using the sorted list in my final calculate.  Obvious and quick fix.

# Day 02: `idris/Day02.idr`

## Task 1: 28m

Straight-forward.  Took the opportunity to play around with some partial functions and have a look in the language to see if there was any way of dealing with them in an exception-catch type of way but I'd have to go to monads for that.  Took the effort to separate things out a bit as this feels bigger than normal for day 02 so I think it might get continued/come back.

## Task 2: 9m

...and of course, something unexpected changes that meaning the design needs changed :).  Instead of refactoring to have proper strategy representation, I just immediately calculate the strategy-appropriate move and return that.  If a later puzzle requires the strategy I'll just have to expand it out then.

# Day 03: `idris/Day03.idr`

## Task 1: 40m 

Simple but interesting task.  Even though I'm doing other stuff at the same time (music, videos, going on tangents in the documentation), I'm going pretty slow with tasks so far this year.  I'm getting particularly annoyed by this laptop keyboard again, making tons of typos, so might switch to an external keyboard for the next couple of days to see if it's that or my typing skills have atrophied.

### Wrong Attempts

28m, 29m, 31m.  Priority calculations wrong; offset meant I wrongly started at 0 and also forgot that upper case range should start at a different offset.  Last attempt was too high and needed 5 mins debugging to realise `intersect` was returning multiple values if the duplicate appeared more than once on a side.  That was easily fixed once worked out with `nub` to remove duplicates.

## Task 2: 14m

Variation that actually requires less work than the first part.  These aren't uncommon but I always find them slightly...annoying is too strong a word, but it feels like the tasks should be the other way around as the second one doesn't require as much "structure" to the data as the first (just treating it as a single value rather than splitting it in half).  Idris has an `intersectAll` function so this was pretty trivial but I was puzzled by an infinite loop for 5 mins before I realised I didn't have a base case for `findBadges` as I had started it a pure HoF way before and hadn't needed one.

# Day 04: `idris/Day04.idr`

## Task 1: 76m

Each year I do AoC I try to have some sort of objective to make double-use of the time.  Last year it was to better understand the Lexer/Parser modules in the Idris stdlib as the documentation on how to use them was rather lacking if you didn't already understand them.  I made a start but then work meant I didn't make it very far through AoC and stopped.  This is a nice trivial parsing problem so I spent the first 70 mins playing around with that to relearn the modules.  The objective is to work out a much, much briefer way to define lexer/parsers for simple cases like this task than having to write out all the quite redundant feeling code that is currently needed.

The underlying motivation for doing this is that ad-hoc string processing can get quite tortuously verbose in a language that worries a lot about lengths of things, indexes being in-bounds, and if lists are empty or not.  If a lexer/parser can be set up quickly then it could be quicker and neater than dealing with lots of bounds checks - the parser either fails or returns the data already chopped up and placed into the datatype you want.

After the lexer/parser play was done, the rest only took 6 mins.  I do tend to deliberately over-engineer solutions nowadays - a few years ago, when I was in a timezone that gave me free time when the tasks were released, I was doing speed-coding solutions (all shortcuts, no error handling) in Haskell that started at a couple of minutes going to ~25m near the end of the month, but I get more fun out of AoC nowadays writing nicer programs with proper data representations, conversions between them, and error handling.  That helps identify things which take a while to do properly, which I might want to work on later speeding up with a different API, convenience module, etc.  And you can do it while half-concentrating on other things and not feel you've messed up a time :).

## Task 2: 17m

Pretty simple part 2, just need to change the overlapping function.  I made a couple of careless variable typos that compared the wrong values then just re-wrote it to check if they don't overlap and invert it.

### Wrong attempts

8m too high, 9m too low.  Using wrong variables in wrong places in the overlapping Boolean expression.  Took a moment to draw a quick diagram but was still getting a too-low answer so re-wrote it to check for no overlap and invert.
