||| Various useful functions to Advent of Code tasks.
module AOCUtils

import System.File
import Data.String
import Data.Maybe
import Data.List

||| Load the file, split it into lines, cast it into type a, pass it to the processing
||| function which returns Maybe b, cast that back to a String if it's not Nothing and print it.
||| The motivation for this is to hide away some of the I/O boilerplate for both reading in
||| the file and of converting the output.
||| The `a` in `func` can be anything that a String can be cast to and the `b` can be anything
||| that can be cast to a String, as shown in the constraint.  This means when dealing with
||| number problems your function can be something like `List Int -> Maybe Int` and not have
||| to worry about Strings at all.
||| It is likely in more complex problems you will have to keep the input as Strings and then
||| do some per-line parsing as the first step of your funtion.
||| Return `Nothing` from `func` to "error out" easily..
export
withLines' : ((Cast String a), (Cast b String)) =>
             (filename : String) -> (func : List a -> Maybe b) -> IO ()
withLines' fname func =
  do Right fstr <- readFile fname | Left err => putStrLn "File read error"
     let res = func $ map cast (lines fstr)
     case res of
          Just x => putStrLn (cast x)
          Nothing => putStrLn "Nothing"

||| withLines' but with the default input filename.
withLines : ((Cast String a), (Cast b String)) => (List a -> Maybe b) -> IO ()
withLines = withLines' "input.txt"

||| Take a 2-ary function and apply it infix to a 3-tuple, left associative.
export
uncurry3infixl : (b -> a -> b) -> (b, a, a) -> b
uncurry3infixl f (x, y, z) = (x `f` y) `f` z

||| Take a 2-ary function and apply it infix to a 3-tuple, right associative.
export
uncurry3infixr : (a -> b -> b) -> (a, a, b) -> b
uncurry3infixr f (x, y, z) = x `f` (y `f` z)

||| `power` for Ints because `pow` doesn't return exact values for integral doubles.
export
pwr : Int -> Int -> Int
pwr x 0 = 1
pwr x 1 = x
pwr x y = x * (pwr x (y-1))

||| Parse a string as the bits of an unsigned int.
export
fromBitString : String -> Int
fromBitString s = fromBitString' (reverse (unpack s)) 0 0
where 
  fromBitString' : List Char -> Int -> Int -> Int
  fromBitString' [] i acc = acc
  fromBitString' ('1' :: xs) i acc = let acc' = acc + (pwr 2 i)
                                     in fromBitString' xs (i+1) acc'
  fromBitString' (_ :: xs) i acc = fromBitString' xs (i+1) acc

||| Maybe get an element from a List.  Mostly to stop me forgetting about
||| `getAt` in the Prelude.
indexMaybe : Nat -> List a -> Maybe a
indexMaybe = getAt

||| Get an element from a List or a default value if the index is out of bounds.
indexDefault : Lazy a -> Nat -> List a -> a
--indexDefault v i xs = fromMaybe v (getAt i xs)
-- Don't know which of these is quicker?
indexDefault v i xs = case inBounds i xs of
                           Yes prf => index i xs
                           _ => v

||| Non-lazy version of (&&) since the laziness in the type causes problems with HOFs.
||| Apparently this is expected behaviour https://github.com/idris-lang/Idris2/issues/1904
||| Would have preferred to call it (&&') but that causes a compiler error and it's not
||| clear from the grammar why it's disallowed (same with (&&")).
export
(&&-) : Bool -> Bool -> Bool
(&&-) a b = a && b

||| Non-lazy version of (||) since the laziness in the type causes problems with HOFs.
||| Apparently this is expected behaviour https://github.com/idris-lang/Idris2/issues/1904
||| Would have preferred to call it (&||) but that causes a compiler error and it's not
||| clear from the grammar why it's disallowed (same with (&&")).
export
(||-) : Bool -> Bool -> Bool
(||-) a b = a || b

infixr 5 &&-
infixr 5 ||-
