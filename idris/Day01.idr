module Day01

import System
import AOCUtils
import Data.List
import Data.List1



task1 : List String -> Maybe Int
task1 [] = Nothing
task1 strs = let perElf = split (== "") strs
                 perElfMax = map (\(xs) => sum (map cast xs)) perElf
             in Just (foldr max 0 perElfMax)



task2 : List String -> Maybe Int
task2 [] = Nothing
task2 strs = let perElf = split (== "") strs
                 perElfMax = forget $ map (\(xs) => sum (map cast xs)) perElf
                 sorted = reverse $ sort perElfMax
             in Just (sum (take 3 sorted))



--------------------------------------------------------------------------------
main1 : IO()
main1 = withLines' "day01-input.txt" task1

main2 : IO()
main2 = withLines' "day01-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
