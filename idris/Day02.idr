module Day02

import System
import AOCUtils
import Data.List
import Data.String

||| Possible RPS hand shapes
data Shape = Rock | Paper | Scissors

||| A Move is made up of each player's shape.
RPSMove : Type
RPSMove = (Shape, Shape)

||| Convert move input into Shape.
partial
parseShape : Char -> Shape
parseShape 'A' = Rock
parseShape 'X' = Rock
parseShape 'B' = Paper
parseShape 'Y' = Paper
parseShape 'C' = Scissors
parseShape 'Z' = Scissors

||| Winner is greater than loser.  EQ on draw.
winner : RPSMove -> Ordering
winner (Rock, Rock) = EQ
winner (Rock, Paper) = LT
winner (Rock, Scissors) = GT
winner (Paper, Rock) = GT
winner (Paper, Paper) = EQ
winner (Paper, Scissors) = LT
winner (Scissors, Rock) = LT
winner (Scissors, Paper) = GT
winner (Scissors, Scissors) = EQ

||| How much each Shape is worth, defined in problem.
shapeScore : Shape -> Int
shapeScore Rock = 1
shapeScore Paper = 2
shapeScore Scissors = 3

||| Score an entire move, defined in problem.
moveScore : RPSMove -> Int
moveScore (x, y) = shapeScore y + case winner (x,y) of
                                       GT => 0
                                       EQ => 3
                                       LT => 6

partial
parseMoves : List String -> List RPSMove
parseMoves [] = []
parseMoves (x :: xs) = let first = parseShape (strIndex x 0)
                           second = parseShape (strIndex x 2)
                       in (first, second) :: parseMoves xs

partial
task1 : List String -> Maybe Int
task1 strs = let mvs = parseMoves strs
                 scrs = map moveScore mvs
                 tot = sum scrs
             in Just tot


--------------------------------------------------------------------------------
||| Given the opponents move and a strategy code, return the appropriate move.
partial
parseShapeForStrategy : Shape -> Char -> Shape
parseShapeForStrategy Rock 'X' = Scissors
parseShapeForStrategy Rock 'Y' = Rock
parseShapeForStrategy Rock 'Z' = Paper
parseShapeForStrategy Paper 'X' = Rock
parseShapeForStrategy Paper 'Y' = Paper
parseShapeForStrategy Paper 'Z' = Scissors
parseShapeForStrategy Scissors 'X' = Paper
parseShapeForStrategy Scissors 'Y' = Scissors
parseShapeForStrategy Scissors 'Z' = Rock

||| When parsing each move, interpret the second char as a strategy and return
||| the appropriate move.
partial
parseMoves2 : List String -> List RPSMove
parseMoves2 [] = []
parseMoves2 (x :: xs) = let first = parseShape (strIndex x 0)
                            second = parseShapeForStrategy first (strIndex x 2)
                        in (first, second) :: parseMoves2 xs

partial
task2 : List String -> Maybe Int
task2 strs = let mvs = parseMoves2 strs
                 scrs = map moveScore mvs
                 tot = sum scrs
             in Just tot

--------------------------------------------------------------------------------
partial
main1 : IO()
main1 = withLines' "day02-input.txt" task1

partial
main2 : IO()
main2 = withLines' "day02-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
partial
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
