module Day03

import System
import AOCUtils
import Data.List
import Data.Nat

Sack : Type
Sack = (List Char, List Char)

parseSack : String -> Sack
parseSack str = let s = unpack str
                    len : Nat = length s
                    mid = (len `div` 2)
                    (s1, s2) = splitAt mid s
                in (s1, s2)

parseSacks : List String -> List Sack
parseSacks strs = map parseSack strs

toPriority : Char -> Int
toPriority c = case isLower c of
                    True => 1 + cast c - cast 'a'
                    False => 27 + cast c - cast 'A'

task1 : List String -> Maybe Int
task1 strs = let sacks = parseSacks strs
                 dups = map (nub . (uncurry intersect)) sacks
                 dups' = concat dups
                 pris = map toPriority dups'
             in Just $ sum pris
             
--------------------------------------------------------------------------------
||| Direct intersect groups of 3 input lines and return the unique duplicate value.
findBadges : List String -> List (List Char)
findBadges [] = []
findBadges strs = let (s1, s2) = splitAt 3 strs
                  in (nub $ intersectAll $ map unpack s1) :: (findBadges s2)

task2 : List String -> Maybe Int
task2 strs = let allBadges = findBadges strs
                 pris = map toPriority (concat allBadges)
             in Just $ sum pris

--------------------------------------------------------------------------------
main1 : IO()
main1 = withLines' "day03-input.txt" task1

main2 : IO()
main2 = withLines' "day03-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
