module Day04

import System
import AOCUtils
import Data.List
import Data.String
import Data.List1
import Text.Lexer
import Text.Parser
import Data.Either

||| Represent a work pairing just as a pair of pairs.
WorkPairing : Type
WorkPairing = ((Int, Int), (Int, Int))

--- Build a lexer
||| The kinds of tokens we want to be able to differentiate
data WorkTokenKind
 = WNum                         -- the numbers
 | WMinus                       -- the '-' used to separate the ranges
 | WComma                       -- the ',' between the pairs
 
||| Eq instance to tell the tokens apart.  Unfortunately Idris doesn't have an
||| automatic deriving mechanism for Eq yet making trivial interfaces verbose.
Eq WorkTokenKind where
  (==) WNum WNum = True
  (==) WMinus WMinus = True
  (==) WComma WComma = True
  (==) _ _ = False

||| Ability to print out the token names.
Show WorkTokenKind where
  show WNum = "WNum"
  show WMinus = "WMinus"
  show WComma = "WComma"

||| Alias to shorten type below
WorkToken : Type
WorkToken = Token WorkTokenKind

||| Ability to print out the token matches in the lexer.
Show WorkToken where
  show (Tok kind text) = "Tok kind: " ++ show kind ++ " text: " ++ text

||| For each token, The `TokType` line say what type of data will be returned
||| with the token and the `tokValue` lines describe how to convert from String
||| to that data type.
TokenKind WorkTokenKind where
  TokType WNum = Int            -- WNum tokens have an associated Int
  TokType _ = ()                -- All other tokens have no data

  tokValue WNum n = cast n      -- Convert from String -> Int with cast
  tokValue WMinus _ = ()        -- No function needed
  tokValue WComma _ = ()        -- No function needed

||| What the lexer should actually match for each token type.
||| If any data should be returned (as described above) this will automatically
||| be captured by the lexer functions and passed to the appropriate token
||| constructors above.  The available matching functions are in Text.Lexer.
workTokenMap : TokenMap WorkToken
workTokenMap = toTokenMap [
  (exact ",", WComma),
  (exact "-", WMinus),
  (digits, WNum)
  ]

||| Run the lexer for our tokens and error handle the results.
lexWork : String -> Maybe (List (WithBounds WorkToken))
lexWork str = case lex workTokenMap str of
                   (tokens, _, _, "") => Just tokens
                   _ => Nothing

--- Buld a minimal parser
||| How the grammer should actually match tokens.  In this case a simple
||| single sequence of matches that are all required & won't fail (on valid data).
||| The data is then returned in whatever format you want to specify
||| as the final parameter to `Grammar`.
workPair : Grammar state WorkToken True WorkPairing
workPair = do
  aOpen <- match WNum
  match WMinus
  aClose <- match WNum
  
  match WComma
  
  bOpen <- match WNum
  match WMinus
  bClose <- match WNum
  
  pure $ ((aOpen, aClose), (bOpen, bClose))

||| Run the parse with our grammar and deal with the error handling.
parseWork1 : List (WithBounds WorkToken) -> Either String WorkPairing
parseWork1 toks = case parse workPair toks of
                       Right (l, []) => Right l
                       Right e => Left "Parsing did not consume all tokens."
                       Left e => Left (show e)

||| Run our lexer and parser together and return the results.
parseWork : String -> Either String WorkPairing
parseWork str = case lexWork str of
                     Just toks => parseWork1 toks
                     Nothing => Left "Failed to lex."

--- Rest of task 1
||| Return true if one range is entirely contained in the other.
containsRange : WorkPairing -> Bool
containsRange ((a1, a2), (b1, b2)) = let aInb = a1 >= b1 && a2 <= b2
                                         bIna = b1 >= a1 && b2 <= a2
                                     in aInb || bIna


task1 : List String -> Maybe Nat
task1 strs = let prs = map parseWork strs
                 (bads, goods) = partitionEithers prs
             in case isNil bads of
                     False => Nothing -- Shouldn't be any bads, so stop the program.
                     True => Just (length $ filter (== True) $ map containsRange goods)


--------------------------------------------------------------------------------
--- Task 2

||| Return true if one range is partially contained in the other.
--- Checks if they don't overlap and inverts.
anyOverlap : WorkPairing -> Bool
anyOverlap ((a1, a2), (b1, b2)) = not $ a2 < b1 || a1 > b2

task2 : List String -> Maybe Nat
task2 strs = let prs = map parseWork strs
                 (bads, goods) = partitionEithers prs
             in case isNil bads of
                     False => Nothing -- Shouldn't be any bads, so stop the program.
                     True => Just (length $ filter (== True) $ map anyOverlap goods)
--------------------------------------------------------------------------------
main1 : IO()
main1 = withLines' "day04-input.txt" task1

main2 : IO()
main2 = withLines' "day04-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
